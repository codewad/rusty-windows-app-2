#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_light_color;
layout (location = 2) in vec2 in_texture_coord;

layout (location = 0) out vec3 light_color;
layout (location = 1) out vec2 texture_coord;

void main() {
    texture_coord = in_texture_coord;
    light_color = in_light_color;
    gl_Position = vec4(in_position, 1.0);
}
