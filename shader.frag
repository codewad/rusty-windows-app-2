#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (set = 0, binding = 0) uniform sampler2D tex_sampler;

layout (location = 0) in vec3 light_color;
layout (location = 1) in vec2 texture_coord;

layout (location = 0) out vec4 outColor;

void main() {
  vec4 tex_color = texture(tex_sampler, texture_coord);
  outColor = vec4(light_color, 1.0) * tex_color;
  //outColor = tex_color;
}
