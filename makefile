SOURCE := $(wildcard src/*.c) $(wildcard src/*.rs) $(wildcard src/**/*.c) $(wildcard src/**/*.rs)

EXE_NAME := hydraulic-erosion.exe
DEBUG_TARGET := ../target/x86_64-pc-windows-gnu/debug/$(EXE_NAME)
RELEASE_TARGET := ../target/x86_64-pc-windows-gnu/release/$(EXE_NAME)
VERTEX_SHADER_TARGET := vert.spv
FRAGMENT_SHADER_TARGET := frag.spv
COMPUTE_SHADER_TARGET := compute.spv

debug: $(DEBUG_TARGET) $(VERTEX_SHADER_TARGET) $(FRAGMENT_SHADER_TARGET) $(COMPUTE_SHADER_TARGET)
	cp $^ bin/

release: $(RELEASE_TARGET) $(VERTEX_SHADER_TARGET) $(FRAGMENT_SHADER_TARGET) $(COMPUTE_SHADER_TARGET)
	cp $^ bin/

clean:
	cargo clean
	rm $(VERTEX_SHADER_TARGET) $(FRAGMENT_SHADER_TARGET) $(COMPUTE_SHADER_TARGET) || true

check:
	cargo check --target=x86_64-pc-windows-gnu

$(DEBUG_TARGET): $(SOURCE)
	cargo build --target=x86_64-pc-windows-gnu

$(RELEASE_TARGET): $(SOURCE)
	cargo build --release --target=x86_64-pc-windows-gnu

$(VERTEX_SHADER_TARGET): shader.vert
	glslc -o $@ $^

$(FRAGMENT_SHADER_TARGET): shader.frag
	glslc -o $@ $^

$(COMPUTE_SHADER_TARGET): shader.compute
	glslc -fshader-stage=compute -o $@ $^
