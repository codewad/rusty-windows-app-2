use lodepng::Bitmap;
use lodepng::RGBA;

pub struct TileMapCoords
{
    pub top: f32,
    pub bot: f32,
    pub right: f32,
    pub left: f32
}

pub struct TileMap
{
    image: Bitmap<RGBA>,
    size_x: usize,
    size_y: usize
}

impl TileMap
{
    pub fn new(image: Bitmap<RGBA>, size_x: usize, size_y: usize) -> Self
    {
        Self
        {
            image: image,
            size_x: size_x,
            size_y: size_y
        }
    }

    pub fn get_image(&self) -> &Bitmap<RGBA>
    {
        &self.image
    }

    pub fn get_coordinates(&self, tile_x: usize, tile_y: usize) -> Option<TileMapCoords>
    {
        if tile_x >= self.size_x
        {
            return None;
        }

        if tile_y >= self.size_y
        {
            return None;
        }

        Some(TileMapCoords {
            top: tile_y as f32 / self.size_y as f32,
            bot: (tile_y + 1) as f32 / self.size_y as f32,
            left: tile_x as f32 / self.size_x as f32,
            right: (tile_x + 1) as f32 / self.size_x as f32
        })
    }
}
