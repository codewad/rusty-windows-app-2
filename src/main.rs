extern crate lodepng;
extern crate rusty_math;
extern crate rusty_windows;
extern crate rand;

mod tilemap;

use std::sync::Arc;
use std::sync::mpsc::{channel, Sender, Receiver, RecvTimeoutError};
use std::thread::JoinHandle;
use std::thread;
use std::time::{Duration, Instant};
use std::collections::HashSet;

//use rand::Rng;
//use lodepng::ColorType;
use rusty_math::ogl::{Mat4, Vec3};
use rusty_windows::input_types::{WindowMessage, Key};
use rusty_windows::windows::{Window, SenderCollection};
use rusty_windows::vulkan;
//use itertools::Itertools;
use vulkan::LogicalDeviceBuilder;

use tilemap::TileMap;

fn make_window_thread() -> (Arc<Window>, JoinHandle<()>)
{
    let (tmp_send, tmp_recv) : (Sender<Arc<Window>>, Receiver<Arc<Window>>) = channel();

    let window_thread = thread::spawn(move || {
        let window_r = Arc::new(Window::new());
        tmp_send.send(Arc::clone(&window_r)).unwrap();
        window_r.message_loop();
    });

    (tmp_recv.recv().unwrap(), window_thread)
}

#[derive(Debug)]
enum ExitCondition
{
    //MessageQueueDisconnection,
    //Quit
}

struct TimeoutMessageQueueIterator<'a>
{
    message_queue: &'a Receiver<WindowMessage>,
    duration: Duration,
    start: Instant
}

impl<'a> TimeoutMessageQueueIterator<'a>
{
    fn new(queue: &'a Receiver<WindowMessage>, duration: Duration) -> Self
    {
        Self
        {
            message_queue: queue,
            duration: duration,
            start: Instant::now()
        }
    }
}

impl<'a> Iterator for TimeoutMessageQueueIterator<'a>
{
    type Item = Result<WindowMessage, RecvTimeoutError>;

    fn next(&mut self) -> Option<Self::Item>
    {
        let elapsed = self.start.elapsed();
        if elapsed < self.duration
        {
            Some(self.message_queue.recv_timeout(self.duration - elapsed))
        }
        else
        {
            None
        }
    }
}

fn load_bytes_from_file(filename : &str) -> Vec<u8>
{
    std::fs::read(filename).unwrap()
}

struct ImagesInFlightTracker<'a, 'b, 'c, 'd, 'e>
{
    in_flight_fences : Vec<Option<&'e vulkan::Fence<'a, 'b, 'c, 'd>>>
}

impl ImagesInFlightTracker<'_, '_, '_, '_, '_>
{
    fn new(count: usize) -> Self
    {
        let mut fences = Vec::with_capacity(count);

        for _i in 0..count
        {
            fences.push(None);
        }

        Self
        {
            in_flight_fences: fences
        }
    }

    fn wait_on_index(&self, index: usize)
    {
        if let Some(fence) = self.in_flight_fences[index]
        {
            fence.wait();
        }
    }
}

impl<'a, 'b, 'c, 'd, 'e> ImagesInFlightTracker<'a, 'b, 'c, 'd, 'e>
{
    fn set(&mut self, index: usize, fence: &'e vulkan::Fence<'a, 'b, 'c, 'd>)
    {
        self.in_flight_fences[index] = Some(fence);
    }
}

fn copy_to_buffer_allocation<T>(allocation: &vulkan::MemoryAllocation, data: &Vec<T>)
{
    let size = std::mem::size_of::<T>() * data.len();
    copy_raw_data_to_buffer(
        allocation,
        data.as_ptr() as *const std::ffi::c_void,
        size
    );
}

fn copy_raw_data_to_buffer(allocation: &vulkan::MemoryAllocation, data: *const std::ffi::c_void, size: usize)
{
    allocation.get_map(size).copy_to(data, size);
}

const FRAMES_IN_FLIGHT : usize = 2;
const TILE_SIZE : f32 = 100.0;

#[repr(C)]
struct UniformObject
{
    model: Mat4<f32>
}

#[repr(C)]
#[derive(Debug)]
pub struct Vertex
{
    pub position : Vec3<f32>,
    pub light: Vec3<f32>,
    pub texture_coords : [f32; 2]
}

fn create_tilemap_mesh(tilemap: &TileMap, window_size_x: i32, window_size_y: i32) -> (Vec<Vertex>, Vec<u16>)
{
    let tile_size_x = TILE_SIZE / window_size_x as f32;
    let tile_size_y = TILE_SIZE / window_size_y as f32;

    let mut vertices: Vec<Vertex> = Vec::new();
    let mut indices: Vec<u16> = Vec::new();

    for x in -5..5 as isize
    {
        for y in -5..5 as isize
        {
            let from_x = x as f32 * tile_size_x;
            let from_y = y as f32 * tile_size_y;
            let index_position = vertices.len() as u16;
            let tilemap_coords = tilemap.get_coordinates(((x + y).abs() % 2) as usize, ((x + y).abs() % 3) as usize).unwrap();

            vertices.push(Vertex {
                position: Vec3::from([from_x, from_y, 0.0]),
                light: Vec3::from([1.0, 1.0, 1.0]),
                texture_coords: [tilemap_coords.left, tilemap_coords.top]
            });
            vertices.push(Vertex {
                position: Vec3::from([from_x + tile_size_x, from_y, 0.0]),
                light: Vec3::from([1.0, 1.0, 1.0]),
                texture_coords: [tilemap_coords.right, tilemap_coords.top]
            });
            vertices.push(Vertex {
                position: Vec3::from([from_x + tile_size_x, from_y + tile_size_y, 0.0]),
                light: Vec3::from([1.0, 1.0, 1.0]),
                texture_coords: [tilemap_coords.right, tilemap_coords.bot]
            });
            vertices.push(Vertex {
                position: Vec3::from([from_x, from_y + tile_size_y, 0.0]),
                light: Vec3::from([1.0, 1.0, 1.0]),
                texture_coords: [tilemap_coords.left, tilemap_coords.bot]
            });

            indices.push(index_position);
            indices.push(index_position + 1);
            indices.push(index_position + 2);
            indices.push(index_position);
            indices.push(index_position + 2);
            indices.push(index_position + 3);
        }
    }

    (vertices, indices)
}

fn _find_queue_family_index<F>(families: &Vec<vulkan::VkQueueFamilyProperties>, condition: F) -> Option<usize>
where F: FnMut(&vulkan::VkQueueFamilyProperties) -> bool
{
    families.iter()
        .position(condition)
}

fn _find_queue_family_index_by_flag(families: &Vec<vulkan::VkQueueFamilyProperties>, flag: vulkan::types::VkQueueFlagBits, without_flag: vulkan::types::VkQueueFlagBits) -> Option<usize>
{
    println!("find_queue_family_index_by_flag");
    println!("Looking for flag: {:#018b}", flag);
    println!("    without flag: {:#018b}", without_flag);
    families.iter().for_each(|family| {
        println!("family: {:#018b}", family.queueFlags);
    });

    if let Some(index) = _find_queue_family_index(
        families,
        |family| { family.queueFlags == flag }
    ) {
        Some(index)
    }
    else if let Some(index) = _find_queue_family_index(
        families,
        |family| {
            family.queueFlags & flag != 0
                && family.queueFlags & without_flag == 0
        }
    ) {
        Some(index)
    }
    else
    {
        _find_queue_family_index(
            families,
            |family| {
                family.queueFlags & flag != 0
            }
        )
    }
}

fn main_loop(window: &Window, message_queue: Receiver<WindowMessage>)
{
    let (window_size_x, window_size_y) = window.get_size();

    let tilemap = TileMap::new(lodepng::decode32_file("tilemap.png").unwrap(), 2, 3);
    let image = tilemap.get_image();

    let (vertices, indices) = create_tilemap_mesh(&tilemap, window_size_x, window_size_y);

    let vertex_buffer_size = std::mem::size_of::<Vertex>() * vertices.len();
    let index_buffer_size = std::mem::size_of::<u16>() * indices.len();
    let image_buffer_size = std::mem::size_of::<lodepng::RGBA>() * image.buffer.len();
    let compute_output_buffer_size = std::mem::size_of::<u32>() * 64;

    //let uniform_buffer_size = std::mem::size_of::<UniformObject>();
    let lib = vulkan::get_library();
    let instance = lib.get_instance().unwrap();
    let _debug_utils = instance.get_debug_utils_callback_instance();
    let surface = instance.get_surface(window.get_handle());

    let physical_devices = instance.get_physical_devices();
    let physical_device = physical_devices.iter()
        .filter(|&device| {
            let properties = device.get_properties();
            println!("physical device properties: {:?}", properties);
            properties.deviceType == vulkan::types::VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU
        })
        .take(1)
        .next()
        .unwrap();

    let queue_families = physical_device.get_queue_families();

    println!("queue_family_indices: {:?}", queue_families);

    let builder = LogicalDeviceBuilder::new(queue_families)
        .with_extension(b"VK_KHR_swapchain\0".to_vec())
        .with_graphics_and_compute_queue()
        .with_surface_support(&physical_device, &surface);

    let graphics_queue_index = builder.get_graphics_queue_index().unwrap();
    let compute_queue_index = builder.get_compute_queue_index().unwrap();

    let (extensions, queue_family_indices) = builder.get_extensions();

    let logical_device = physical_device.make_logical_device(
        extensions,
        &queue_family_indices
    );

    let graphics_queue : vulkan::Queue = logical_device.get_queue(graphics_queue_index as u32);
    let compute_queue : vulkan::Queue = logical_device.get_queue(compute_queue_index as u32);
    let command_pool = logical_device.create_command_pool(graphics_queue_index as u32);

    let vertex_buffer = logical_device.create_vertex_buffer(vertex_buffer_size);
    let vertex_buffer_allocation = vertex_buffer.get_local_allocation();
    vertex_buffer_allocation.bind().unwrap();

    let vertex_staging_buffer = logical_device.create_staging_buffer(vertex_buffer_size);
    let vertex_staging_buffer_allocation = vertex_staging_buffer.get_staging_allocation();
    vertex_staging_buffer_allocation.bind().unwrap();

    let index_buffer = logical_device.create_index_buffer(index_buffer_size);
    let index_buffer_allocation = index_buffer.get_local_allocation();
    index_buffer_allocation.bind().unwrap();

    let texture_image = logical_device.create_texture_image(image.width, image.height);
    let texture_allocation = texture_image.get_allocation();
    texture_allocation.bind().unwrap();

    let mut keys_pressed : HashSet<Key> = HashSet::new();

    {
        copy_to_buffer_allocation(&vertex_staging_buffer_allocation, &vertices);

        let index_staging_buffer = logical_device.create_staging_buffer(index_buffer_size);
        let index_staging_buffer_allocation = index_staging_buffer.get_staging_allocation();
        index_staging_buffer_allocation.bind().unwrap();

        copy_to_buffer_allocation(&index_staging_buffer_allocation, &indices);

        let image_staging_buffer = logical_device.create_staging_buffer(image_buffer_size);
        let image_staging_buffer_allocation = image_staging_buffer.get_staging_allocation();
        image_staging_buffer_allocation.bind().unwrap();

        copy_to_buffer_allocation(&image_staging_buffer_allocation, &image.buffer);

        let command_buffer = logical_device.create_command_buffer(&command_pool);
        command_buffer.begin_one_time_recording()
            .copy_buffer(&vertex_staging_buffer, &vertex_buffer, vertex_buffer_size)
            .copy_buffer(&index_staging_buffer, &index_buffer, index_buffer_size)
            .pipeline_barrier_to_dst_optimal(&texture_image)
            .copy_buffer_to_image(
                &image_staging_buffer,
                &texture_image,
                image.width as u32,
                image.height as u32
            )
            .pipeline_barrier_to_shader_read_only(&texture_image);
        graphics_queue.simple_submit(&command_buffer);
        graphics_queue.wait_idle();
    }

    let texture_image_view = logical_device.create_image_view(&texture_image, vulkan::types::VK_FORMAT_R8G8B8A8_SRGB);
    let texture_sampler = logical_device.create_sampler();

    let surface_capabilities = physical_device.get_surface_capabilities(&surface);
    let surface_formats = physical_device.get_surface_formats(&surface);

    let selected_surface_format = match surface_formats.iter()
        .filter(|format| -> bool { format.format == vulkan::types::VK_FORMAT_B8G8R8A8_SRGB && format.colorSpace == vulkan::types::VK_COLOR_SPACE_SRGB_NONLINEAR_KHR })
        .nth(0)
    {
        Some(x) => x,
        None => surface_formats.iter().nth(0).unwrap()
    };

    let swap_chain = logical_device.create_swap_chain(
        &surface,
        selected_surface_format,
        vulkan::types::VK_PRESENT_MODE_MAILBOX_KHR,
        &queue_family_indices,
        &surface_capabilities
    );
    let render_pass = logical_device.create_render_pass(selected_surface_format);
    let depth_image = logical_device.create_depth_image(swap_chain.get_width(), swap_chain.get_height());
    let depth_image_memory = depth_image.get_allocation();
    depth_image_memory.bind().unwrap();

    let depth_image_view = logical_device.create_depth_image_view(&depth_image, physical_device.find_depth_format().unwrap());
    let framebuffers = swap_chain.create_framebuffers(&depth_image_view, &render_pass);

    let mut descriptor_set_definition = vulkan::DescriptorSetDefinition::new();
    descriptor_set_definition.add_image_sampler();
    let descriptor_set_layout = logical_device.create_descriptor_set_layout(&descriptor_set_definition);
    let pipeline_layout = logical_device.create_pipeline_layout(&descriptor_set_layout);

    let vertex_shader_module = logical_device.create_shader_module(&load_bytes_from_file("vert.spv"));
    let fragment_shader_module = logical_device.create_shader_module(&load_bytes_from_file("frag.spv"));
    let graphics_pipeline = logical_device.create_graphics_pipeline::<Vertex>(
        &vertex_shader_module,
        &fragment_shader_module,
        &(surface_capabilities.currentExtent),
        &pipeline_layout,
        &render_pass
    );

    let compute_shader_module = logical_device.create_shader_module(&load_bytes_from_file("compute.spv"));
    let mut compute_descriptor_set_definition = vulkan::DescriptorSetDefinition::new();
    compute_descriptor_set_definition.add_storage_buffer();
    compute_descriptor_set_definition.add_dynamic_storage_buffer();
    println!("compute descriptor set definition: {:?}", compute_descriptor_set_definition);

    let compute_descriptor_set_layout = logical_device.create_descriptor_set_layout(&compute_descriptor_set_definition);
    let compute_pipeline_layout = logical_device.create_pipeline_layout(&compute_descriptor_set_layout);
    let compute_pipeline = logical_device.create_compute_pipeline(&compute_shader_module, &compute_pipeline_layout);

    let compute_input_storage_buffers : Vec<vulkan::Buffer> = framebuffers.iter().flat_map(
        |_| {
            vec![
                logical_device.create_storage_buffer(64 * std::mem::size_of::<u32>())
            ]
        }).collect();

    let compute_input_buffer_allocations : Vec<vulkan::MemoryAllocation> = compute_input_storage_buffers.iter()
        .map(|b| {
            let allocation = b.get_staging_allocation();
            allocation.bind().unwrap();
            allocation
        }).collect();

    let compute_output_local_buffers : Vec<vulkan::Buffer> = framebuffers.iter().flat_map(
        |_| {
            vec![
                logical_device.create_src_storage_buffer(compute_output_buffer_size)
            ]
        }).collect();

    let compute_output_local_allocations : Vec<vulkan::MemoryAllocation> = compute_output_local_buffers.iter()
        .map(|b| {
            let allocation = b.get_staging_allocation();
            allocation.bind().unwrap();
            allocation
        }).collect();

    let compute_output_buffers : Vec<vulkan::Buffer> = framebuffers.iter().flat_map(
        |_| {
            vec![
                logical_device.create_dest_storage_buffer(compute_output_buffer_size)
            ]
        }).collect();

    let compute_output_allocations : Vec<vulkan::MemoryAllocation> = compute_output_buffers.iter()
        .map(|b| {
            let allocation = b.get_staging_allocation();
            allocation.bind().unwrap();
            allocation
        }).collect();

    let command_buffers : Vec<vulkan::CommandBuffer> = framebuffers.iter().map(
        |_| {
            logical_device.create_command_buffer(&command_pool)
        }).collect();

    let compute_command_buffers : Vec<vulkan::CommandBuffer> = framebuffers.iter().map(
        |_| {
            logical_device.create_command_buffer(&command_pool)
        }).collect();

    // let uniform_buffers : Vec<vulkan::Buffer> = framebuffers.iter().flat_map(
    //     |_| {
    //         vec![
    //             logical_device.create_uniform_buffer(uniform_buffer_size)
    //         ]
    //     }).collect();

    // let uniform_buffer_allocations : Vec<vulkan::MemoryAllocation> = uniform_buffers.iter()
    //     .map(|b| {
    //         let allocation = b.get_staging_allocation();
    //         allocation.bind().unwrap();
    //         allocation
    //     }).collect();

    let descriptor_pool = logical_device.create_descriptor_pool(framebuffers.len(), &descriptor_set_definition);
    println!("graphics descriptor pool: {:?}", descriptor_pool);
    let compute_descriptor_pool = logical_device.create_descriptor_pool(framebuffers.len(), &compute_descriptor_set_definition);
    println!("compute descriptor pool: {:?}", compute_descriptor_pool);

    let descriptor_set_layouts : Vec<&vulkan::DescriptorSetLayout> = vec![
        &descriptor_set_layout,
        &descriptor_set_layout
    ];

    let mut descriptor_sets = descriptor_pool.create_descriptor_sets(&descriptor_set_layouts);

    {
        let mut descriptor_set_iterator = descriptor_sets.iter_mut();

        while let Some(set) = descriptor_set_iterator.next()
        {
            set.sampler(&texture_sampler, &texture_image_view);
            set.update();
        }
    }

    let compute_descriptor_set_layouts = vec![
        &compute_descriptor_set_layout,
        &compute_descriptor_set_layout
    ];

    let mut compute_descriptor_sets = compute_descriptor_pool.create_descriptor_sets(&compute_descriptor_set_layouts);

    {
        let mut compute_descriptor_set_iterator = compute_descriptor_sets.iter_mut();
        let mut compute_input_buffers = compute_input_storage_buffers.iter();
        let mut compute_output_buffers = compute_output_local_buffers.iter();

        while let (
            Some(set),
            Some(input_buffer),
            Some(output_buffer)) = (
            compute_descriptor_set_iterator.next(),
            compute_input_buffers.next(),
            compute_output_buffers.next()
        )
        {
            set.storage_buffer(&input_buffer, 32);
            set.dynamic_storage_buffer(&output_buffer, compute_output_buffer_size);
            set.update();
        }
    }

    let image_available_semaphores = logical_device.create_multiple_semaphores(FRAMES_IN_FLIGHT);
    let render_finished_semaphores = logical_device.create_multiple_semaphores(FRAMES_IN_FLIGHT);
    let fences = logical_device.create_multiple_fences(FRAMES_IN_FLIGHT);
    let compute_fences = logical_device.create_multiple_fences(FRAMES_IN_FLIGHT);

    {
        let mut command_buffer_iter = command_buffers.iter();
        let mut frame_buffer_iter = framebuffers.iter();
        let mut descriptor_set_iter = descriptor_sets.iter();
        let mut compute_command_buffer_iter = compute_command_buffers.iter();
        let mut compute_descriptor_set_iter = compute_descriptor_sets.iter();
        let mut compute_output_read_buffers_iter = compute_output_buffers.iter();
        let mut compute_output_local_buffers_iter = compute_output_local_buffers.iter();

        while let (Some(command_buffer),
                   Some(frame_buffer),
                   Some(descriptor_set),
                   Some(compute_command_buffer),
                   Some(compute_descriptor_set),
                   Some(compute_output_buffer),
                   Some(compute_output_read_buffer)) =
            (command_buffer_iter.next(),
             frame_buffer_iter.next(),
             descriptor_set_iter.next(),

             compute_command_buffer_iter.next(),
             compute_descriptor_set_iter.next(),
             compute_output_local_buffers_iter.next(),
             compute_output_read_buffers_iter.next())
        {
            println!("compute command buffer begin");
            compute_command_buffer.begin_recording()
                .bind_compute_pipeline(&compute_pipeline)
                .bind_descriptor_set_for_compute_pipeline(&compute_pipeline_layout, &compute_descriptor_set, 0)
                .dispatch(64, 1, 1)
                .copy_buffer(&compute_output_buffer, &compute_output_read_buffer, compute_output_buffer_size)
                .barrier_buffer_compute_to_transfer(compute_output_buffer, compute_output_buffer_size as u64, graphics_queue_index, graphics_queue_index);
            println!("compute command buffer end");

            println!("command buffer begin");
            command_buffer.begin_recording()
                .begin_render_pass(
                    &render_pass,
                    &frame_buffer,
                    &(surface_capabilities.currentExtent)
                )
                .bind_graphics_pipeline(&graphics_pipeline)
                .bind_descriptor_set_for_graphics_pipeline(&pipeline_layout, &descriptor_set, 0)
                .bind_vertex_buffer(&vertex_buffer)
                .bind_index_buffer(&index_buffer)
                .draw_indexed(indices.len() as u32, 1, 0, 0, 0)
                .end_render_pass();

            println!("command buffer end");
        }
    }

    println!("Entering main loop");
    let mut current_frame : usize = 0;
    let mut images_in_flight = ImagesInFlightTracker::new(FRAMES_IN_FLIGHT);

    loop
    {
        for message_result in TimeoutMessageQueueIterator::new(&message_queue, Duration::from_millis(17))
        {
            match message_result
            {
                Ok(message) => match message
                {
                    WindowMessage::Keydown { key: Some(x) } => { keys_pressed.insert(x); },
                    WindowMessage::Keyup { key: Some(x) } => { keys_pressed.remove(&x); },
                    _ => { println!("Real Window: {:?}", message); }
                },
                Err(RecvTimeoutError::Timeout) => { },
                Err(x) => {
                    println!("Unknown error when listening for messages: {:?}", x);
                    logical_device.wait_idle();
                    window.close();
                    return;
                }
            }
        }

        for key in keys_pressed.iter()
        {
            match key
            {
                Key::Escape => {
                    println!("Quitting from command");
                    logical_device.wait_idle();
                    window.close();
                    return;
                }
        //         Key::KW => { camera_offset = camera_offset + &relative_z_axis; }
        //         Key::KS => { camera_offset = camera_offset - &relative_z_axis; }
        //         Key::KA => { camera_offset = camera_offset + &relative_x_axis; }
        //         Key::KD => { camera_offset = camera_offset - &relative_x_axis; }
        //         Key::KR => { camera_offset = camera_offset + &relative_up_axis; }
        //         Key::KF => { camera_offset = camera_offset - &relative_up_axis; }
        //         Key::KQ => { z_rotation = rotate_around_vector(relative_z_axis, 0.01); }
        //         Key::KE => { z_rotation = rotate_around_vector(relative_z_axis, -0.01); }
                 _ => {}
            }
        }

        // let model_matrix = Mat4::from([
        //     1.0, 0.0, 0.0, 0.0,
        //     0.0, 1.0, 0.0, 0.0,
        //     0.0, 0.0, 1.0, 0.0,
        //     0.0, 0.0, 0.0, 1.0
        // ]);

        {
            let fence = &fences[current_frame];
            let compute_fence = &compute_fences[current_frame];
            //let uniform_buffer_allocation = &uniform_buffer_allocations[current_frame];
            let image_available_semaphore = &image_available_semaphores[current_frame];
            let render_finished_semaphore = &render_finished_semaphores[current_frame];

            // let uniform_data = UniformObject
            // {
            //     model: model_matrix
            // };

            fence.wait();
            compute_fence.wait();

            let image_index = swap_chain.acquire_next_image(&image_available_semaphore);
            let command_buffer = &command_buffers[image_index as usize];
            let compute_command_buffer = &compute_command_buffers[image_index as usize];

            images_in_flight.wait_on_index(image_index as usize);
            images_in_flight.set(image_index as usize, fence);

            let mut value : [u32; 64] = [1000; 64];
            println!("before({:?}): {:?}", current_frame, value);

            let compute_output_buffer_map = compute_output_allocations[current_frame].get_map(compute_output_buffer_size);
            compute_output_buffer_map.copy_from(value.as_mut_ptr() as *mut std::ffi::c_void, compute_output_buffer_size);
            println!("after({:?}): {:?}", current_frame, value);

            compute_fence.reset();
            fence.reset();

            graphics_queue.submit(
                command_buffer,
                vec![image_available_semaphore],
                vec![render_finished_semaphore],
                Some(fence)
            );

            compute_queue.submit(
                compute_command_buffer,
                vec![],
                vec![],
                Some(compute_fence)
            );

            graphics_queue.present(&swap_chain, &render_finished_semaphore, image_index);
        }

        current_frame = (current_frame + 1) % FRAMES_IN_FLIGHT;
    }
}

fn main()
{
    let mut sender_collection = SenderCollection::new();
    sender_collection.make_context();

    let (window_send, window_recv) : (Sender<WindowMessage>, Receiver<WindowMessage>) = channel();
    let (window_r, window_thread) = make_window_thread();
    sender_collection.register(&window_r, window_send);

    main_loop(&window_r, window_recv);
    println!("Real Window, exiting main loop");

    let _result = window_thread.join();
    println!("quitting now");
}
